#include "BB8RemoteController.h"
#include "utils.h"

#include <ros/ros.h>
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/JoyFeedbackArray.h"
#include "sensor_msgs/JoyFeedback.h"
#include "sensor_msgs/BatteryState.h"
#include "sensor_msgs/Joy.h"

namespace bb8_remote_controller {


BB8RemoteController::BB8RemoteController(ros::NodeHandle& nh, ros::NodeHandle& nh_private):
    nh_(nh),
    nh_private_(nh_private)
{
    // read in config
    nh_private_.param("feedback_topic", feedbackTopic_,
        std::string("/set_feedback"));
    nh_private_.param("battery_state_topic", batteryStateTopic_,
        std::string("/battery_state"));
    nh_private_.param("joy_wiimote_topic", joyWiimoteTopic_,
        std::string("/joy"));
    nh_private_.param("joy_nunchuk_topic", joyNunchukTopic_,
        std::string("/wiimote/nunchuk"));
    nh_private_.param("motor_command_topic", motorCommandTopic_,
        std::string("/motor_command"));
    
    // internal state
    alerted_ = false;

    // stuff for system feedback
    feedbackPublisher_ = nh_.advertise<sensor_msgs::JoyFeedbackArray>(feedbackTopic_, 1);
    batteryStateSubscriber_ = nh_.subscribe(batteryStateTopic_, 1,
        &BB8RemoteController::batteryStateCallback, this);
    disableBuzzerTimer_ = nh.createTimer(ros::Duration(5.0),
            &bb8_remote_controller::BB8RemoteController::disableBuzzerTimed,
            this,true,false);
    
    
    // stuf for controlling actuators
    joyWiimoteSubscriber_ = nh_.subscribe(joyWiimoteTopic_, 1,
        &bb8_remote_controller::BB8RemoteController::joyWiimoteCallback, this);
    joyNunchukSubscriber_ = nh_.subscribe(joyNunchukTopic_, 1,
        &bb8_remote_controller::BB8RemoteController::joyNunchukCallback, this);
    motorCommandPublisher_ = nh_.advertise<geometry_msgs::Twist>(motorCommandTopic_, 1);;
    
}

void BB8RemoteController::batteryStateCallback(const sensor_msgs::BatteryState& batteryStateMsg){
    
    sensor_msgs::JoyFeedbackArray outMsg;
    // wiimote has 4 LEDs, use them as battery level meter
    outMsg.array.resize(4);
    for(uint32_t i = 0; i<outMsg.array.size(); i++){
        outMsg.array[i].type = sensor_msgs::JoyFeedback::TYPE_LED;
        outMsg.array[i].type = i;
        outMsg.array[i].intensity = 0.0;
    }
    if(batteryStateMsg.percentage>=0.8){
        outMsg.array[3].intensity = 1.0;
    }
    if(batteryStateMsg.percentage>=0.6){
        outMsg.array[2].intensity = 1.0;
    }
    if(batteryStateMsg.percentage>=0.4){
        outMsg.array[1].intensity = 1.0;
    }
    if(batteryStateMsg.percentage>=0.2){
        outMsg.array[0].intensity = 1.0;
        // reset alerted state when charge is/was over 20 percent
        // so we alert again when it falls under 0.2 again
        alerted_ = false;
    }

    feedbackPublisher_.publish(outMsg);

    if((batteryStateMsg.percentage<0.2) & (!alerted_)){
        sendBatteryAlert();
    }
}

void BB8RemoteController::sendBatteryAlert(){
    enableBuzzer();
    disableBuzzerTimer_.start();
}

void BB8RemoteController::enableBuzzer(){
    sensor_msgs::JoyFeedbackArray outMsgAry;
    sensor_msgs::JoyFeedback outMsg;
    outMsg.type = sensor_msgs::JoyFeedback::TYPE_RUMBLE;
    outMsg.id = 0;
    outMsg.intensity = 1.0;

    outMsgAry.array.push_back(outMsg);
    feedbackPublisher_.publish(outMsg);
}

void BB8RemoteController::disableBuzzerTimed(const ros::TimerEvent& timerEvent){
    disableBuzzer();
}

void BB8RemoteController::disableBuzzer(){
    sensor_msgs::JoyFeedbackArray outMsgAry;
    sensor_msgs::JoyFeedback outMsg;
    outMsg.type = sensor_msgs::JoyFeedback::TYPE_RUMBLE;
    outMsg.id = 0;
    outMsg.intensity = 0.0;

    outMsgAry.array.push_back(outMsg);
    feedbackPublisher_.publish(outMsg);
}

void BB8RemoteController::joyWiimoteCallback(const sensor_msgs::Joy& joyWiimoteMsg){
    // TODO do stuff with pressed buttons and stuff
}
void BB8RemoteController::joyNunchukCallback(const sensor_msgs::Joy& joyNunchukMsg){
    geometry_msgs::Twist motorMsg;
    // pressing joystick away from oneself gives [1, 0]
    // pressing joystick to the right gives [0, -1]
    // 
    // pressing joystick to the far right corner gives [0.75, -0.75],
    // so adding dead-zone at 0.75

    motorMsg.linear.x = clamp(-0.75f, 0.75f, joyNunchukMsg.axes[0])*(1.0f/0.75f);
    motorMsg.angular.z = clamp(-0.75f, 0.75f, joyNunchukMsg.axes[1])*(1.0f/0.75f);

    motorCommandPublisher_.publish(motorMsg);

    // TODO do stuff with pressed buttons and stuff

}

} //namespace
