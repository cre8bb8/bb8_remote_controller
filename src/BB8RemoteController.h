#ifndef BB8_REMOTE_CONTROLLER_BB8_REMOTE_CONTROLLER_H
#define BB8_REMOTE_CONTROLLER_BB8_REMOTE_CONTROLLER_H

#include <ros/ros.h>

#include "sensor_msgs/Joy.h"
#include "sensor_msgs/BatteryState.h"

namespace bb8_remote_controller{

    class BB8RemoteController {
        public:
            BB8RemoteController(ros::NodeHandle& nh, ros::NodeHandle& nh_private);

        private:
            ros::NodeHandle nh_;
            ros::NodeHandle nh_private_;

            // stuff for system feedback
            ros::Publisher  feedbackPublisher_;
            std::string feedbackTopic_;
            ros::Subscriber batteryStateSubscriber_;
            std::string batteryStateTopic_;

            void batteryStateCallback(const sensor_msgs::BatteryState& batteryStateMsg);

            // stuf for controlling actuators
            ros::Subscriber joyWiimoteSubscriber_;
            std::string joyWiimoteTopic_;
            ros::Subscriber joyNunchukSubscriber_;
            std::string joyNunchukTopic_;
            ros::Publisher  motorCommandPublisher_;
            std::string motorCommandTopic_;

            void joyWiimoteCallback(const sensor_msgs::Joy& joyWiimoteMsg);
            void joyNunchukCallback(const sensor_msgs::Joy& joyNunchukMsg);

            void sendBatteryAlert();
            void enableBuzzer();
            void disableBuzzer();
            void disableBuzzerTimed(const ros::TimerEvent& timerEvent);
            ros::Timer disableBuzzerTimer_;

            // internal state
            
            bool alerted_;
    };
} // Namespace

#endif // include guard

