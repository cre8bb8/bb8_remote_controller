#include <ros/ros.h>
#include "BB8RemoteController.h"

int main( int argc, char** argv )
{
    ros::init(argc, argv, "bb8_remote_controller");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    bb8_remote_controller::BB8RemoteController BB8RemoteController(nh, nh_private);

    ros::spin();

    return 0;
}

