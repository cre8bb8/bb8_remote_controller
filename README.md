# BB8 Remote Controller

ROS node that allows manual remote operation of BB8.
It sits in between a remote control node (e.g. wiimote) and the chassis controller.


## Subscriptions

* sensor\_msgs/Joy: Wiimote and Nunchuk
* sensor\_msgs/BatteryState: battery state


## Publications

* geometry\_msgs/Twist: Motor commands
* sensor\_msgs/JoyFeedbackArray: Battery state and alert for low battery to wiimote

## Implementation Details

To allow the absolute strongest accelerations (translational as well as rotational),
we had to get rid of the dead zone in the Nunchuk joystick.

A recorded trace during which I moved the joystick multiple times along the outer
edge,
reveals that the raw values exactly represent the mask around the joystick:

![Trace](calibration/trace.png)

Raw data (including the accelerometer): [download](calibration/raw.csv)

So I clamped both axes at 0.75 and streched them by multiplying with 1.0/0.75.

## Links

* [Project Wiki](https://bitbucket.org/cre8bb8/doc/wiki/Home)

